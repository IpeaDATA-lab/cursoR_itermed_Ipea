library(foreign)
base <- read.spss("bancos/electric.sav")
base.df <- as.data.frame(base)

install.packages("RSQLite")
conn <- DBI::dbConnect(RSQLite::SQLite(), dbname = "bancos/chinook.db")

tables.list <- DBI::dbListTables(conn)

## última tabela
tail(tables.list, 1)
tables.list[length(tables.list)]

tracks <- DBI::dbGetQuery(conn, "select * from tracks")
tracks.df = as.data.frame(tracks)

library(dplyr)
library(knitr)
generos <- tracks.df%>%group_by(GenreId)%>%summarise(distintos = n(), media = mean(Milliseconds), CV = sd(UnitPrice)/mean(UnitPrice))

data(iris)
iris %>% dplyr::select(-c(3,4)) %>% dplyr::group_by(Species) %>% mutate(Sepal.Ratio = Sepal.Length/Sepal.Width) %>% filter(Sepal.Ratio > 1.85 && Sepal.Ratio <2) %>% summarise(n.long.Sepal = n()) %>% knitr::kable()

