---
title: "aula_13_out"
author: "Vanessa Nadalin"
date: "13 de outubro de 2017"
output: word_document
---

criar objeto com meu nome


```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}



library(stringr)
nome <- c("Vanessa")
stringr::str_length(nome)
nome_completo <- c("Vanessa Gapriotti Nadalin")

nome_separado <- c("Vanessa", "Gapriotti", "Nadalin")
stringr::str_length(nome_completo)
stringr::str_length(nome_separado)

```


Agora vai carregar um banco de dados que contem o poema cançao do exílio


```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}

getwd()
classico <- read.table("C:\\Users\\treinamento\\Desktop\\CursoR\\cursoR_itermed_Ipea\\cursoR_itermed_Ipea\\bancos\\Aula 4\\cancao_do_exilio.txt")

```

Criar coluna chamada caracteres, com o número de caracteres em cada verso.


```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}
library(stringr)
classico$caracteres <- str_length(classico$verso)
classico <- classico %>%
  dplyr::mutate(caracteres2=str_length(verso))


```

No R o jeito de fazer loop, dizer para fazer para cada linha é o comando apply. Por linha ou por coluna? Por linha o parâmetro é 1.

```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}
classico$caracter3 <- apply(as.data.frame(as.character(classico$verso)),1,str_length)

```




Trabalhando com a função *apply*:
```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}
dados <- as.data.frame(matrix(rnorm(5000,10,5),nrow=1000,ncol=5))
somalinhas <- apply(dados,1,sum)
dados$soma <- somalinhas
mediacolunas <- apply(dados, 2,mean)
normaliza_colunas <- apply(dados,2,function(x) (x-min(x))/(max(x)-min(x)))
knitr::kable(mediacolunas)
```

Funções dentro da função *apply*

```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}

func_colunas <- apply(dados,2,function(x) (sum(x))/(sum(x^2)))
knitr::kable(mediacolunas)
```


Como concaternar strings:

```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}
str_c("ABC","abc","123")

str_c("ABC","abc","123", sep=" ")

str_c("ABC","abc","123", sep="qualquercoisa")
lista_palavras <- c("informa","atualiza","organiza","ocupa")
str_c("Des",lista_palavras,"do")
str_c(str_c("Des",lista_palavras,"do"),collapse='. ')
```


Criando coluna com resultado de análise do número total de caracteres, se par ou ímpar

```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}

classico$preferencia <- ifelse(classico$caracteres%%2==0,
                              str_c(classico$caracteres, " é um número legal"), 
                              str_c("não gosto do número", classico$caracteres))
classico$preferencia2 <- apply(classico,1,
                      function(x) ifelse(as.numeric(x[2])%%2==0,
                                  str_c(x[2], " é um número legal"), 
                                  str_c("não gosto do número", x[2])))
classico <- classico %>%
    dplyr::mutate(preferencia3=ifelse(caracteres%%2==0,
                              str_c(classico$caracteres, " é um número legal"), 
                              str_c("não gosto do número", classico$caracteres)))

```

É possível separar uma string em vários pedaços com a função str_split

```{r, echo=TRUE, eval=TRUE, warning=FALSE, message=FALSE}
library(stringr)
texto <- "5351073 Brasilia Cidade Maravilhosa"
split <- as.data.frame(str_split(texto, pattern=" "))
teste <- t(split)
teste2 <- c(teste[,1],str_c(teste[,2:4], collapse= " "))




palavras <- str_split(classico$verso,pattern=" ")
str(palavras)
```

