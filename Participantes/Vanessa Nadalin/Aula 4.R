# importando bases com pacote foreign

library("foreign")


setwd("C:\\Users\\treinamento\\Desktop\\CursoR\\cursoR_itermed_Ipea")

base.sav <- foreign::read.spss("bancos/electric.sav")
base.sav <- as.data.frame(base.sav)
base.sav <- foreign::read.spss("bancos/electric.sav", to.data.frame=TRUE)

#conexão SQL pacote do tidyverse: DBI
# query consulta

# vamos ler com o pacote SQLite

#1. faz a conexão e guarda em um objeto essa conexão

#2. sintaxe para ler que tabelas tem la dentro
#tem quais bancos estao la, as relações entre os bancos e views: os labels das variaveis fator.

#3. Query, a consulta, trzer o objeto para R, para dataframe do R

#4. desconectar, porque so pode uma pessoa usar por vez

#install.packages("SQLite")

conn <- DBI::dbConnect(RSQLite::SQLite(),
                       dbname="bancos/chinook.db")

str(conn)
# listar o nome das tabelas presentes nessa conexão
tables <-  DBI::dbListTables(conn)

# query:selecionar todas as linhas da ultima tabela

tail(tables)
ult.tabela <- tables[length(tables)]
codigo.sql <- "SELECT * FROM tracks"
tracks.df <- RSQLite::dbGetQuery(conn, codigo.sql)
tracks.df <- RSQLite::dbGetQuery(conn, paste0("SELECT * FROM ",tables[length(tables)]))

# total de musicas por gÊnero 

library(dplyr)
mus_gen <- tracks.df %>%
            group_by(GenreId, MediaTypeId) %>%
              summarise(distintos=n(),
                        media=mean(Milliseconds))

# com coeficiente de variação
mus_gen <- tracks.df %>%
  group_by(GenreId) %>%
  summarise(distintos=n(),
            media=mean(Milliseconds),
            CV=sd(UnitPrice)/mean(UnitPrice))



#para resolver problema que pede toda hora senha
# ssh-keygen -t rsa -c "vanenaga@yahoo.com.br"
# vim id_rsa.pub
# aparece a codificação da sua senha.
################################

data("iris")
iris.df <- iris

result <-  iris %>% 
            dplyr:: select(-c(3,4)) %>%
            dplyr:: group_by(Species) %>%
            dplyr:: mutate(Sepal.Ratio=Sepal.Length/Sepal.Width, ln.length=log(Sepal.Length)) %>%
            dplyr:: filter(Sepal.Ratio>1.85)%>%
            dplyr:: summarise(n.long.Sepal=n())%>%
            knitr::kable()
  

plot(result)
install.packages(psych)