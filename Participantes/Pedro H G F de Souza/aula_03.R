# Exemplo: Histograma

## Distribuicao normal, 1000 obs, media 10, desvio padrao 3
dados <- rnorm(1000,10,3)
## Histograma nativo
hist(dados)

# caso o git nao funcione por algum motivo: limpa as credenciais e faz novo push
# git config --system --unset credential.helper


# Instalar o pacote devtools
install.packages("devtools")

# Instalar microdadosBrasil
## se usar o prefixo devtools:: antes do install_github nao precisa carregar o pacote antes.
library("devtools")
install_github("lucasmation/microdadosBrasil")


# Exercicio
## 1. Com o pacote readr leia o dicionario da PNAD
install.packages("readr")
library("readr")
dic <- readr::read_csv("bancos/dicionario_pessoas.csv")
### versao point n click : dic <- readr::read_csv(file.choose())

## 2. Com esse dicionario, use a funcao read_fwf() para ler o arquivo da PNAD 2009.
## Crie o col_positions atraves da funcao fwf_widths, onde o argumento widths eh igual
## a coluna tamanho2 do dicionario.str(widt)
pnad2009.df <- readr::read_fwf("bancos/AMOSTRA_DF_PNAD2009p.txt", 
                               col_positions = 
                                 readr::fwf_widths(dic$tamanho2, col_names = dic$cod2) )

# Obs: pacote Hmisc cria data frames com variable labels. 

## 3. Agora leia o mesmo arquivo usando o pacote microdadosBrasil
pnad2009_microBr.df <- microdadosBrasil::read_PNAD(ft = "pessoas", i = 2009, 
                                                   file = "bancos/AMOSTRA_DF_PNAD2009p.txt")


# Pacote readxl: util porque planilhas excel sao complicadas, com varias abas etc. 
install.packages("readxl")

# Exercicio
## 1. Quais os nomes das planilhas no datasets.xls?
abas <- readxl::excel_sheets("bancos/datasets.xls")
print(sheets.datasets.xls)
## 2. Leia a terceira planilha desse documento.
chickwts.df <- readxl::read_excel("bancos/datasets.xls", sheet = sheets.datasets.xls[3])
### pode usar tambem sheet = 3.
## 3. Crie um loop com lapply para ler todas as planilhas de uma só vez. 
files <- "bancos/datasets.xls"
planilhas <- lapply(readxl::excel_sheets(files),
                    function(x) readxl::read_excel(files, sheet = x))
str(planilhas)
iris <- planilhas[[1]]
mtcars <- planilhas[[2]]
chickwts <- planilhas[[3]]
quakes <- planilhas[[4]]
## primeiro argumento eh a lista de nomes, o segundo eh a funcao

# Append de todos os elementos da lista
for(i in 1:length(planilhas)) {
  final <- rbind(final,planilhas[[i]]) 
}


