---
title: "Aula 05"
author: "Pedro H.G.F. de Souza"
date: "10 de outubro de 2017"
output: word_document
---

Bla bla bla 

```{r warning=FALSE, message=FALSE, echo=FALSE}

# Pacotes 
#install.packages("nycflights13")
library(dplyr)
library(nycflights13)
library(knitr)
data("flights")
# names("flights")
# View("flights")

# Lista de destinos unicos por aviao
# flights %>%
#  select(tailnum, dest) %>%
#  group_by(tailnum) %>%
#  summarise(ndest = n_distinct(dest)) %>%
#  filter(ndest== 1) %>%
#  kable()

# Destinos mais visitados
flights %>%
    group_by(carrier, dest) %>%
    summarise(n = n()) %>%
    mutate(rank = rank(desc(n))) %>%
    filter(rank == 1) %>%
    head(n = 3) %>%
    arrange(desc(n)) %>%
    kable()

# Encontre o numero de voos que nao sao NA e viraram o dia (departure > arrival)
total<- flights %>%
  filter(!is.na(dep_time) & dep_time > arr_time) %>%
  summarize(n_flights = n()) 

```

O total de vôos é de `r total` -- vôos que não são NA e viraram o dia.


## Comecando a trabalhar com *gather()*

```{r warning=FALSE, message=FALSE, echo=FALSE}

# head(mtcars,5) -- fica tabela feia
kable(head(mtcars,5), caption = "Primeiras linhas de mtcars")

```

Estudando a função **gather()**:

```{r warning=FALSE, message=FALSE, echo=FALSE}
mtcars %>%
  tidyr::gather("peca", "n", cyl, gear, carb) %>%
  dplyr::group_by(peca) %>%
  dplyr::summarise(n_distintos = n_distinct(n)) %>%
  knitr::kable()
  
```

A função spread() é o oposto de gather()

```{r warning=FALSE, message=FALSE, echo=FALSE}
mtcars %>%
  tidyr::gather("peca", "n", cyl, gear, carb) %>%
  dplyr::group_by(peca) %>%
  dplyr::summarise(n_distintos = n_distinct(n)) %>%
  tidyr::spread(peca, n_distintos) %>%
  knitr::kable()
  
```



```{r warning=FALSE, message=FALSE, echo=FALSE}

data("starwars")

starwars %>%
  tidyr::gather("tipo","cor", dplyr::ends_with("color")) %>%
  dplyr::group_by(tipo) %>%
  summarise(cores_distintas = n_distinct(cor)) %>%
  kable()

# Filtre o genero para male/female , separe a coluna em duas, com valor height

dplyr::starwars %>%
  dplyr::filter(gender %in% c("male","female")) %>%
  tidyr::spread(gender, height) %>%
  dplyr::select(male,female) %>%
  head() %>%
  kable()

```


Funções *unite* e  *separate*

```{r warning=FALSE, message=FALSE, echo=FALSE}

# com o banco flights, crie uma coluna chamada trajetos que tem o formato origin-dest, depois
# descubra o # de trajetos distintos por cia aerea

data("flights")

flights %>%
  tidyr::unite("trajetos", origin, dest, sep = " - ") %>%
  dplyr::group_by(carrier) %>%
  dplyr::summarise(trajetos_distintos = n_distinct(trajetos)) %>%
  arrange(desc(trajetos_distintos)) %>%
  kable()


```

