/* PROGRAMA DE LEITURA EM SAS DO ARQUIVO DE MICRODADOS DE STATUS DE IMPUTACAO 
   DE DOMIC�LIO DA PNAD 2015 - PESQUISA B�SICA E SUPLEMENTAR DE ACESSO � INTERNET E � TELEVIS�O
	
Vari�veis "S" - Vari�veis de Status
Vari�veis "V" - Vari�veis originais
0 - Registro n�o Imputado
1 - Registro imputado

 */

DATA DOMSTATUS2015;
INFILE '...\Marca de imputa��o\Dados\DOMSTATUS2015.TXT' LRECL=66 MISSOVER;
INPUT  
        @00001     V0101      $4. 		
        @00005     V0102      $8. 		
        @00013     V0103      $3. 		
        @00016     S0201      $1. 		
        @00017     S0202      $1. 		
        @00018     S0203      $1. 		
        @00019     S0204      $1. 		
        @00020     S0205      $1. 		
        @00021     S0206      $1. 		
        @00022     S0207      $1. 		
        @00023     S0208      $1. 		
        @00024     S0209      $1. 		
        @00025     S0210      $1. 		
        @00026     S0211      $1. 		
        @00027     S0212      $1. 		
        @00028     S0213      $1. 		
        @00029     S0214      $1. 		
        @00030     S0215      $1. 		
        @00031     S0216      $1. 		
        @00032     S2016      $1. 		
        @00033     S0217      $1. 		
        @00034     S0218      $1. 		
        @00035     S0219      $1. 		
        @00036     S0220      $1. 		
        @00037     S2020      $1. 		
        @00038     S0221      $1. 		
        @00039     S0222      $1. 		
        @00040     S0223      $1. 		
        @00041     S0224      $1. 		
        @00042     S0225      $1. 		
        @00043     S0226      $1. 		
        @00044     S0227      $1. 		
        @00045     S02270     $1. 		
        @00046     S02271     $1. 		
        @00047     S02272     $1. 		
        @00048     S02273     $1. 		
        @00049     S02274     $1. 		
        @00050     S2027      $1. 		
        @00051     S0228      $1. 		
        @00052     S0229      $1. 		
        @00053     S0230      $1. 		
        @00054     S0231      $1. 		
        @00055     S0232      $1. 		
        @00056     S02321     $1. 		
        @00057     S02322     $1. 		
        @00058     S02323     $1. 		
        @00059     S02324     $1. 		
        @00060     S02325     $1. 		
        @00061     S02326     $1. 		
        @00062     S02327     $1. 		
        @00063     S02424     $1. 		
        @00064     S02425     $1. 		
        @00065     S02426     $1. 		
        @00066     S2032      $1. 		
 

;

		 run;