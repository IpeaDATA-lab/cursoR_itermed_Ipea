---
title: "Aula 5 - Dplyr"
author: "Lucas Vasconcelos"
date: "05 de outubro de 2017"
output: word_document
---

# Exercício A3P8.1

Selecione as colunas Sepal.Length, Sepal.Width, Petal.Length e Petal.Width do banco iris. Tente fazer de 3 formas diferentes.

* A primeira forma de selecionar as variáveis pode ser feita da seguinte maneira:

```{r,eval=TRUE,echo=TRUE,warning=FALSE,message=FALSE}
library(dplyr)
data(iris)
head(iris %>%
  select(Sepal.Length, Sepal.Width, Petal.Length, Petal.Width))
```

* A segunda forma de selecionar as variáveis pode ser feita da seguinte maneira (`results='hide'` esconde o resultado):

```{r,eval=TRUE,echo=TRUE,warning=FALSE,message=FALSE,results='hide'}
select <- iris %>%
  select(1:4)
```

* A terceira forma de selecionar as variáveis pode ser feita da seguinte maneira:

```{r,eval=TRUE,echo=TRUE,warning=FALSE,message=FALSE}
head(iris %>%
  select(-5))
```

# Como colocar um *chunk* no meio do texto

O tamanho médio das pétalas é `r round(mean(iris$Sepal.Length),2)` o que é um tamanho razoável.

# Exemplo A3P10

```{r,eval=TRUE,echo=TRUE}
iris %>%
  dplyr::mutate(glob_sd = sd(Petal.Width)) %>%
  dplyr::group_by(Species) %>%
  dplyr::mutate(Species_sd = sd(Petal.Width)) %>%
  dplyr::summarise(glob_value = unique(glob_sd),
                   specie_value = unique(Species_sd)) %>%
  knitr::kable(digits = 2)
```

# Exercício A3P11.2-3

```{r,eval=TRUE,echo=TRUE}
# 2
# install.packages("nycflights13")

# 3
library(nycflights13)
data("flights")
nrow(flights)
head(flights %>% 
  select(tailnum, dest) %>% 
  group_by(tailnum) %>% 
  summarise(dist=n_distinct(dest)) %>% 
  filter(dist==1))
```

