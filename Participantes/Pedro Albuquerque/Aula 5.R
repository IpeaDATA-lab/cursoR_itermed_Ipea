## Leitura da PNAD
devtools::install_github("lucasmation/microdadosBrasil")
add<-"Participantes/Pedro Albuquerque"

chunk EVAL=FA

#Fazemos somente uma vez caso os dados não existam localmente
download_sourceData("PNAD", 2015, root_path = add)

#Lê os dados da PNAD
pnad <- read_PNAD("pessoas", 2015, root_path = add)

save.image("dados.RData")
#Renda V9532

load("dados.RData")


#Lê os dados da PNAD
pnad <- read_PNAD("pessoas", 2015, root_path = add)
#Exemplo Média do Rendimento
library(dplyr)
pnad %>%
  group_by(UF) %>%
  filter(V9532<999999999)%>%
  summarise(weighted.mean(V9532 , 
                          w = V4729, 
                          na.rm = TRUE)
  ) -> agg.df


#Cor V0404

#Exemplo frequência COR/Raça

pnad %>%
  group_by(UF, V0404) %>%
  summarise (n.cor = sum(V4729))%>%
  group_by(UF) %>%
  mutate(freq = n.cor/n) %>%
  mutate(freq2 = paste0(round(freq,6)*100,"%")) -> cor.df

pnad %>%
  group_by(UF, V0404) %>%
  summarise (n.cor = sum(V4729))%>%
  group_by(UF) %>%
  mutate (n = sum(n.cor)) %>%
  mutate(freq = n.cor/n) %>%
  mutate(freq2 = paste0(round(freq,6)*100,"%")) -> cor.df



cor.df$Cor = factor(cor.df$V0404,
                    levels=c(2,4,6,8,0,9),
                    labels=c("Branca","Preta","Amarela",
                             "Parda","Indígena","Sem declaração"),
                    ordered=FALSE)
knitr::kable(cor.df)

library(tidyverse)
cor.df2<- cor.df %>%
  select(UF,freq,Cor)%>%
  group_by(UF)%>%
  spread(Cor,freq)

cor.df[is.na(cor.df)]<-0
rownames(cor.df2)<-cor.df2$UF
cor.df2<-cor.df2[,-1]

apply(cor.df2,1,function(x) max(x,na.rm=T))

#Exemplo Gini
library(reldist)
pnad %>%
  group_by(UF) %>%
  filter(V9532<999999999)%>%
  summarise(gini.idx=gini(V9532,V4729))


#######################################################################
#######################################################################
#######################################################################


### Exercício 1

ind_uso <- sapply(pnad, function(x){
  mean(is.na(x)) < 0.2
})
nomes <- names(pnad)[ind_uso]

### Exercício 2

pnad %>% 
  select(nomes) %>% 
  group_by(get("V0302")) %>% 
  summarise(n = n())


### Exercício 3

pnad %>% 
  select(nomes) %>% 
  group_by(get("V0408")) %>% 
  summarise(n = n())

### Exercício 4

ler_func <- function(col_tab){
  pnad %>% 
    select(nomes) %>% 
    group_by(get("col_tab")) %>% 
    summarise(n = n())
}

### Exercício 5

lapply(c("V0302", "V0408", "V0608", "V6112"), ler_func)


