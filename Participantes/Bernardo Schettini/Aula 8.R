#Lê os dados
classico.df<-read.table("bancos/Aula 4/cancao_do_exilio.txt")
library(stringr)
#Faz o split em lista
classico.list<-str_split(classico.df$verso, pattern = " ")
#Cria as matrizes
indx <- sapply(classico.list, length)
res <- do.call(rbind,lapply(classico.list, 'length<-',max(indx)))
#Converte para data.frame
res.df<-as.data.frame(res)

nome_completo <- c("Bernardo Patta Schettini")
str_split(nome_completo, pattern="i|o")

classico.df$numero <- str_length(classico.df$verso)
classico.df$divisao <- classico.df$numero%/%as.numeric(rownames(classico.df))
classico.df$pos.modernismo <- ifelse(classico.df$numero%%7==0,"Sem alteração",NA)
classico.df$pos.modernismo[is.na(classico.df$pos.modernismo)] <- str_replace_all(classico.df$verso[is.na(classico.df$pos.modernismo)],pattern=" ",replacement=as.character(classico.df$divisao[is.na(classico.df$pos.modernismo)]))

# Maísculas x minúsculas
str_to_upper("organização DAS nações Unidas")
str_to_lower("organização DAS nações Unidas")
str_to_title("organização DAS nações Unidas")

# Encoding
iconv("organização DAS nações Unidas", to='ASCII//TRANSLIT')

# Expressões regulares (regex)
library(htmlwidgets)
hierarquia <- c("Soldado","Cabo","Sargento","Tenente","Capitão","Major","Coronel")
str_view(hierarquia,pattern="nt")
str_view(hierarquia,pattern=".nt.")
str_view(hierarquia,pattern="^C")
str_view(hierarquia,pattern="o$")

lista <- c("Rio Grande do Sul","Rio de Janeiro","Cabo Frio","Três Rios","Rio", "São José do Rio Preto")
str_view(lista, pattern = "Rio")
str_view(lista, pattern = "^Rio$")

# Nomes sadefd
library(dplyr)
base<-readRDS("bancos/Aula 4/nomesBR.rds")
base <- mutate(base,vogais=str_count(nome,"[AEIOU]"),consoantes=str_count(nome,"[^AEIOU]"))
convencional <- base[str_detect(base$nome,"^[AEIOU](.*)[^AEIOU]$") & base$sexo==2,]

str_view(hierarquia, pattern = "d.+")
str_view(hierarquia, pattern = ".o*")
str_view(hierarquia, pattern = "it?")
