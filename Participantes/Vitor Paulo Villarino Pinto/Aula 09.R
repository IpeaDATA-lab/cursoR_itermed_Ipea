#Aula 09
#Datas

library(lubridate)

data <- ymd('2013-1-3')
str(data)

# ymd_hms() para hora minuto e segundo
referencia <- ymd_hms("1994-08-18 20:18:18")
update(referencia, year = 2025, month = 4, hour = 17)

referencia <- ymd_hms("1994-08-18 20:18:18")
referencia + months(3) + minutes(10)

#referencia - years(2) - hours(5)

#Exercício 1
wday(today() - days(96500))

#Exercício 2
wday(today() + years(3) + months(7) + seconds(1800))

#Exercício 3
dias <- ymd(20170131) + months(0:11) 
which(!is.na(dias))
